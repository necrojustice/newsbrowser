import { SELECT_NEWS_PAGE,
    INVALIDATE_NEWS_PAGE,
    NEWS_REQUEST,
    NEWS_SUCCESS,
    NEWS_FAILURE } from '../actions/news';

export function selectedNewsPage(state = 1, action) {
  switch (action.type) {
    case SELECT_NEWS_PAGE:
      return action.page;
    default:
      return state;
  }
}

function news(state = {
  isFetching: false,
  didInvalidate: false,
  totalCount: 0,
  news: [],
  error: null,
}, action) {
  switch (action.type) {
    case INVALIDATE_NEWS_PAGE:
      return Object.assign({}, state, {
        didInvalidate: true,
      });
    case NEWS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false,
      });
    case NEWS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        totalCount: action.total_count,
        news: action.news,
        error: null,
      });
    case NEWS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export function newsByPage(state = { }, action) {
  switch (action.type) {
    case INVALIDATE_NEWS_PAGE:
    case NEWS_REQUEST:
    case NEWS_SUCCESS:
    case NEWS_FAILURE:
      return Object.assign({}, state, {
        [action.page]: news(state[action.page], action),
      });
    default:
      return state;
  }
}
