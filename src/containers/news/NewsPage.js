import React, { Component, PropTypes } from 'react';
import shallowCompare from 'react-addons-shallow-compare'
import { connect } from 'react-redux';
import classNames from 'classnames';

import { invalidateNewsPage, selectNewsPage, fetchTopNewsIfNeeded } from '../../actions/news';

import 'react-virtualized/styles.css';
import './news.css'

class NewsPage extends Component {
  constructor(props) {
    super(props);
    this.handleNextPageClick = this.handleNextPageClick.bind(this);
    this.handlePreviousPageClick = this.handlePreviousPageClick.bind(this);
    this.handleRefreshClick = this.handleRefreshClick.bind(this);
    this.getNoRowsRenderer = this.getNoRowsRenderer.bind(this);
    this.getRowClassName = this.getRowClassName.bind(this);
  }

  componentDidMount() {
    const { dispatch, page } = this.props;
    dispatch(fetchTopNewsIfNeeded(page));
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch, page } = nextProps;
    dispatch(fetchTopNewsIfNeeded(page));
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  getNoRowsRenderer() {
    return (
      <div className='noRows'>
        No rows
      </div>
    )
  }

  getRowClassName({ index }) {
    if (index < 0) {
      return 'headerRow';
    }
    return index % 2 === 0 ? 'evenRow' : 'oddRow';
  }

  handleNextPageClick(e) {
    e.preventDefault();
    const { page, news } = this.props;
    if (news.length > 0) {
      // go to next page only if more data may be available
      this.props.dispatch(selectNewsPage(page + 1));
    }
  }

  handlePreviousPageClick(e) {
    e.preventDefault();
    const page = this.props.page;
    if (page > 1) {
      this.props.dispatch(selectNewsPage(page - 1));
    }
  }

  handleRefreshClick(e) {
    e.preventDefault();

    const { dispatch, page } = this.props;
    dispatch(invalidateNewsPage(page));
  }

  render() {
    const { page, error, news, isFetching } = this.props;
    const prevStyles = classNames('page-item', { disabled: page <= 1 });
    const nextStyles = classNames('page-item', { disabled: news.length === 0 });

    let items = news.map(function(repo){
        return <div className="col-sm-6 col-md-4">
                <div className="thumbnail">
                  <img src="http://lorempixel.com/243/200/" alt="image text" />
                  <div className="caption">
                    <h3>{repo.title}</h3>
                    <p>Published: {repo.published}</p>
                  </div>
                </div>
              </div>;
    });

    return (
      <div className="container">

        <nav>
          <ul className="pagination pagination-sm">
            <li className={prevStyles}><a className="page-link" href="#" onClick={this.handlePreviousPageClick}><span>Previous</span></a></li>
            {!isFetching &&
              <li className="page-item" ><a className="page-link" href="#" onClick={this.handleRefreshClick}><span>Refresh page {page}</span></a></li>
            }
            {isFetching &&
              <li className="page-item"><span className="page-link"><i className="fa fa-refresh fa-spin"></i> Refreshing page {page}</span></li>
            }
            <li className={nextStyles}><a className="page-link" href="#" onClick={this.handleNextPageClick}><span>Next</span></a></li>
          </ul>
        </nav>

        {
          error &&
            <div className="alert alert-danger">
              {error.message || 'Unknown errors.'}
            </div>
        }

        {!isFetching && news.length === 0 &&
          <div className="alert alert-warning">Oops, nothing to show.</div>
        }

        {news &&
          <div className="container" style={ { opacity: isFetching ? 0.5 : 1, width: '100%', height: '80vh', position: 'absolute' }}>
            {items}
          </div>  
        }
      </div>
    );
  }
}

NewsPage.propTypes = {
  page: PropTypes.number.isRequired,
  news: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  error: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  left: PropTypes.number,
  top: PropTypes.number,
};

function mapStateToProps(state) {
  const { selectedNewsPage, newsByPage } = state;
  const page = selectedNewsPage || 1;
  if (!newsByPage[page]) {
    return {
      page,
      error: null,
      isFetching: false,
      didInvalidate: false,
      totalCount: 0,
      news: [],
    };
  }

  return {
    page,
    error: newsByPage[page].error,
    isFetching: newsByPage[page].isFetching,
    didInvalidate: newsByPage[page].didInvalidate,
    totalCount: newsByPage[page].totalCount,
    news: newsByPage[page].news,
  };
}

export default connect(mapStateToProps)(NewsPage);
