
import { callApi } from '../utils/apiUtils';

export const SELECT_NEWS_PAGE = 'SELECT_NEWS_PAGE';
export const INVALIDATE_NEWS_PAGE = 'INVALIDATE_NEWS_PAGE';

export const NEWS_REQUEST = 'NEWS_REQUEST';
export const NEWS_SUCCESS = 'NEWS_SUCCESS';
export const NEWS_FAILURE = 'NEWS_FAILURE';

export function selectNewsPage(page) {
  return {
    type: SELECT_NEWS_PAGE,
    page,
  };
}

export function invalidateNewsPage(page) {
  return {
    type: INVALIDATE_NEWS_PAGE,
    page,
  };
}

function newsRequest(page) {
  return {
    type: NEWS_REQUEST,
    page,
  };
}

// This is a curried function that takes page as argument,
// and expects payload as argument to be passed upon API call success.
function newsSuccess(page) {
  return function (payload) {
    return {
      type: NEWS_SUCCESS,
      page,
      news: payload.news,
      totalCount: payload.news.length,
    };
  };
}

// This is a curried function that takes page as argument,
// and expects error as argument to be passed upon API call failure.
function newsFailure(page) {
  return function (error) {
    return {
      type: NEWS_FAILURE,
      page,
      error,
    };
  };
}

const API_ROOT = 'http://www.stellarbiotechnologies.com';

function fetchTopNews(page) {
  let limit = 6;
  let offset = page * limit;

  const url = `${API_ROOT}/media/press-releases/json?limit=${limit}&offset=${offset}`;
  return callApi(url, null, newsRequest(page), newsSuccess(page), newsFailure(page));
}

function shouldFetchNews(state, page) {
  // Check cache first
  const news = state.newsByPage[page];
  if (!news) {
    // Not cached, should fetch
    return true;
  }

  if (news.isFetching) {
    // Shouldn't fetch since fetching is running
    return false;
  }

  // Should fetch if cache was invalidate
  return news.didInvalidate;
}

export function fetchTopNewsIfNeeded(page) {
  return (dispatch, getState) => {
    if (shouldFetchNews(getState(), page)) {
      return dispatch(fetchTopNews(page));
    }
  };
}
